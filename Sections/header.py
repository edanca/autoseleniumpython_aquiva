from selenium.webdriver.common.by import By
from Utils import Common
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoSuchFrameException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import NoAlertPresentException

class Header():

	def get_icon_by_css(self, driver):

		self.icon_flag_selector = "#j_id0\\3a j_id1\\3a j_id7 > div > div > div > div > span"
		
		elem = Common._find_element(driver, By.CSS_SELECTOR, self.icon_flag_selector)

		if elem is None:
			raise NoSuchElementException
		
		return elem


	def get_icon_list_by_css(self, driver):
		pass


	def get_mail_to(self, driver):

		self.mail_element_selector = "body > section > header > div > a"
		
		elem = Common._find_element(driver, By.CSS_SELECTOR, self.mail_element_selector)

		if elem is None:
			raise NoSuchElementException

		return elem