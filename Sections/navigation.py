from selenium.webdriver.common.by import By
from Utils import Common
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoSuchFrameException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import NoAlertPresentException

class Navigation():

	def get_navigation_bar_by_css(self, driver):

		self.navigation_bar_selector = "body > section > nav > div"

		elem = Common._find_element(driver, By.CSS_SELECTOR, self.navigation_bar_selector)

		if elem is None:
			raise NoSuchElementException

		return elem