from selenium import webdriver
from Config import setUp


def getDriver(siteURL = ''):

	globalDriver = webdriver.Chrome()
	globalDriver.get(getURL(siteURL))

	return globalDriver



def getURL(siteURL):

	if siteURL == '':
		return setUp.baseURL

	else:
		return siteURL
