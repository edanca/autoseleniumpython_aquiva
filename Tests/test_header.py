from Tests.test_base import Test_Base
from Utils import Common
from Sections.header import Header
from Config import Chrome
import pytest

class Test_Header():

	@pytest.fixture(scope="module")
	def driver(self):
		print('setting up')
		tb = Test_Base()
		dv = tb.mySetUp()
		return dv


	def test_elements(self, driver):

		self._verify_flag_tag_name(driver)
		self._verify_icon_flag(driver)
		self._verify_main_mail(driver)


	def test_true(driver):
		print(driver)
		assert True


	def _verify_icon_flag(self, driver):
		
		header = Header()
		# Find element
		# start()
		elem = header.get_icon_by_css(driver)
		class_name = 'flag-icon'

		try:
			Common._validate_class(elem, class_name)
			print(f'Icon has class {class_name}')

		except:
			print(f'Icon has no class {class_name}')
			raise


	def _verify_flag_tag_name(self, driver):
		
		header = Header()
		elem = header.get_icon_by_css(driver)
		tag_name = 'span'

		try:
			Common._validate_tag_name(elem, tag_name)
			print(f'Tag name is {tag_name}')

		except:
			print(f'Tag name is not "{tag_name}".')
			raise


	def _verify_main_mail(self, driver):

		header = Header()
		elem = header.get_mail_to(driver)

		email = "aquiva@aquivalabs.com"

		try:
			Common._validate_email_value(elem, email)
			print(f'Email is "{email}"')

		except:
			print(f'Email is not "{email}".')
			raise

