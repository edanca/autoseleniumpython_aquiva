import unittest

from Config import Chrome
from Config import configFile

from selenium.webdriver.support.ui import WebDriverWait


class Test_Base(unittest.TestCase):

	def mySetUp(self):
		self.base_URL = configFile.baseURL
		self.driver = Chrome.getDriver(self.base_URL)

		self.wait = WebDriverWait(self.driver, 5, poll_frequency=1.0)
		
		return self.driver


