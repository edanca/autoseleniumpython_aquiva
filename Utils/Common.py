from Config import setUp
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoSuchFrameException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import NoAlertPresentException
import pytest

def isTextCorrect(text):
	pass


def _find_element(driver, *args):
	
	try:
		return driver.find_element(*args)
	except NoSuchElementException as e:
		raise e
	except WebDriverException as e:
		raise e
	except:
		raise
	

def _find_element_by_id():
	pass


def _validate_class(elem, class_name):
	
	is_flag_icon = class_name in elem.get_attribute("class")

	if is_flag_icon:
		assert True

	return False


def _validate_tag_name(elem, tag_name):	
	
	result = False
	try:
		if elem.tag_name == tag_name:
			result = True

		assert result == True
	except:
		raise

	return result


def _validate_email_value(elem, email):

	result = False
	try:
		if elem.text == email:
			result = True
		
		assert result == True
	except:
		raise

	return result
